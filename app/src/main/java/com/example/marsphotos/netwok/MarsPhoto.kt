package com.example.marsphotos.netwok

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MarsPhoto(val id:String,@SerialName("img_src")val imageSource:String)
